import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

import 'package:inrtu_app/widgets/eventsNews/news_events.dart';
import 'package:inrtu_app/widgets/schedule/schedule.dart';
import 'package:inrtu_app/components/icon_badge.dart';
import 'package:inrtu_app/widgets/settings/settings.dart';

class MainScreen extends StatefulWidget {
  final SharedPreferences sharedStorage;

  MainScreen({Key key, @required this.sharedStorage}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  PageController _pageController;
  int _page = 0;
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    Widget barIcon(
        {IconData icon = Icons.schedule, int page = 0, bool badge = false}) {
      return IconButton(
        icon: badge
            ? IconBadgeWidget(icon: icon, size: 24)
            : Icon(icon, size: 24),
        color: _page == page
            ? Theme.of(context).accentColor
            : Colors.blueGrey[300],

        onPressed: () => _pageController.jumpToPage(page),
      );
    }

    return Scaffold(
//        appBar: AppBar(title: new Text("ИРНИТУ"), actions: <Widget>[
//          IconButton(
//              icon: IconBadgeWidget(icon: Icons.notifications_none),
//              onPressed: () {})
//        ]),
        body: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            onPageChanged: onPageChanged,
            children: <Widget>[
              ScheduleWidget(sharedStorage: widget.sharedStorage),
              NewsEventWidget(),
              SettingsWidget(sharedStorage: widget.sharedStorage)
            ]),
//        bottomNavigationBar: BottomAppBar(
//            child: Row(
//                mainAxisSize: MainAxisSize.max,
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  SizedBox(width: 7),
//
//                  barIcon(icon: Icons.schedule, page: 0),
//                  barIcon(icon: Icons.event, page: 1),
//                  barIcon(icon: Icons.settings, page: 2, badge: true),
//                  SizedBox(width: 7)
//                ]),
//            color: Theme.of(context).primaryColor)
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.schedule),
            title: Text('Расписание'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            title: Text('Новости'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Настройки'),
          ),
        ],
        currentIndex: _selectedIndex,
        backgroundColor:Theme.of(context).primaryColor ,
        selectedItemColor: Colors.lightBlue,
        onTap: _onItemTapped,
      ),
    );
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.jumpToPage(index);
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}
