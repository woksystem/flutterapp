import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:inrtu_app/components/icon_badge.dart';
import 'package:inrtu_app/widgets/eventsNews/categories.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:add_2_calendar/add_2_calendar.dart';

class EventsDetailsWidget extends StatefulWidget {
  final Map eventItem;

  const EventsDetailsWidget({Key key, this.eventItem}) : super(key: key);

  @override
  _EventsDetailsState createState() => _EventsDetailsState();
}

class _EventsDetailsState extends State<EventsDetailsWidget> {
  final GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  @override
  Widget build(BuildContext context) {
    log(DateTime.now().toString());
    Event event = Event(
      title: "${widget.eventItem["name"]}",
      description: '',
      location: '',
      startDate: DateTime.parse(widget.eventItem["starts_at"]).add(Duration(hours: -5)),
      endDate: DateTime.parse(widget.eventItem["ends_at"]).add(Duration(hours: -5)),
      allDay: false,
    );

    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
            actions: <Widget>[
              IconButton(
                icon: IconBadgeWidget(icon: Icons.bookmark),
                onPressed: () {
                  Add2Calendar.addEvent2Cal(event).then((success) {
                    scaffoldState.currentState.showSnackBar(
                        SnackBar(content: Text(success ? 'Success' : 'Error')));
                  });

//
                },
              ),
            ]),
        body: ListView(children: <Widget>[
//          SizedBox(height: 10),
          Container(
            height: 250,
            width: MediaQuery.of(context).size.width,

            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: NetworkImage("${widget.eventItem["poster_image"]}"),
              ),
            ),
          ),

          SizedBox(height: 10),
          ListView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width - 40,
                          alignment: Alignment.centerLeft,
                          child: Text("${widget.eventItem["name"]}",
                              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
                              textAlign: TextAlign.left)),
                    ]),
                SizedBox(height: 5),
                Container(
                  height: 25,
                  child: CategoriesWidget(snapshot: widget.eventItem["categories"]),

                ),
                Container(
                  padding: EdgeInsets.all(5.0),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${widget.eventItem["starts_at_new"]}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                    maxLines: 1,
                    textAlign: TextAlign.left,
                  ),
                ),
//                SizedBox(height: 10),
                Container(
                    child: MaterialButton(
                      color: Colors.blueAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Text('Зарегистрироваться'),
                      onPressed: () {

                        launch( "${widget.eventItem["url"]}");
                      },
                    )
                ),

                Container(
                  child: SingleChildScrollView(
                    child: Html(
                      data: """
               ${widget.eventItem["description_html"]}
                """,
                    ),
                  ),
                ),
                SizedBox(height: 10)
              ])
        ]));
  }
}

