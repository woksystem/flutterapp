import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:inrtu_app/model/event_model.dart';
import 'package:inrtu_app/widgets/events/events_detail.dart';
import 'package:inrtu_app/widgets/eventsNews/categories.dart';

import 'package:inrtu_app/components/message_box.dart';

class EventWidget extends StatefulWidget {
  @override
  _EventState createState() => _EventState();
}

class _EventState extends State<EventWidget> {
  Future<List<dynamic>> _events;
  bool _hasConnect = false;

  @override
  void initState() {
    InternetAddress.lookup('google.com').then((value) {
      try {
        if (value.isNotEmpty && value[0].rawAddress.isNotEmpty) {
          _hasConnect = true;
          _events = getEventFromServer();
          setState(() {});
        }
      } catch (Exception) {
        log("can nit load news");
      }
    }).catchError((error) {
      _hasConnect = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
//      SizedBox(height: 40),
          FutureBuilder<List<dynamic>>(
              future: _events,
              builder: (context, snapshot) {
                if (!_hasConnect) {
                  return new MessageBoxWidget(
                      message: 'Отсутствует подключение к Интернету');
                }
                if (snapshot.hasData) {
                  return new Container(
//                          padding: EdgeInsets.only( left: 10),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      primary: false,
                      itemCount: snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Map eventItem = snapshot.data[index];
                        return Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
//                            color: Colors.black12,
                            child: InkWell(
                              child: Container(
                                height: 346,
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  children: <Widget>[
                                    ClipRRect(
//                                            borderRadius: BorderRadius.circular(5),
                                      child: Image.network(
                                        "${eventItem["poster_image"]}",
                                        height: 170,
                                        width: MediaQuery.of(context).size.width,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    SizedBox(height: 7),
                                    Container(
                                      height: 25,
                                      child: CategoriesWidget(snapshot: eventItem["categories"]),

                                    ),
                                    Container(
                                      padding: EdgeInsets.all(5.0),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "${eventItem["starts_at_new"]}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        ),
                                        maxLines: 1,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
//                                          SizedBox(height: 7),

                                    Container(
                                      padding: EdgeInsets.all(3.0),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "${eventItem["name"]}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                        maxLines: 2,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
//                                          SizedBox(height: 3),
                                    Container(
                                      padding: EdgeInsets.all(3.0),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "${eventItem["description_short"]}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.blueGrey[300],
                                        ),
                                        maxLines: 3,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
//                                          SizedBox(height: 10),

                                  ],
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) {
                                      return EventsDetailsWidget(eventItem: eventItem);
                                    },
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                if (snapshot.hasError) {
                  return new Text("${snapshot.error}");
                }
                return new  SpinKitDoubleBounce(
                  color: Colors.blueAccent,
                  size: 50.0,
                );
              }),
        ]));
  }
}
