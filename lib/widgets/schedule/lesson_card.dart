import 'package:flutter/material.dart';
import 'package:inrtu_app/util/constants.dart';
import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

class LessonCardWidget extends StatefulWidget {
  final bool isNow;
  final bool isDual;
  final String firstLessonName;
  final String secondLessonName;
  final String timeStart;
  final String timeEnd;
  final String firstLessonType;
  final String secondLessonType;
  final String firstLessonTeacher;
  final String secondLessonTeacher;
  final String firstLessonLocation;
  final String secondLessonLocation;
  final bool themes;

  LessonCardWidget(
      {this.isNow,
      this.isDual,
      this.firstLessonName,
      this.secondLessonName,
      this.timeStart,
      this.timeEnd,
      this.firstLessonType,
      this.secondLessonType,
      this.firstLessonTeacher,
      this.secondLessonTeacher,
      this.firstLessonLocation,
      this.secondLessonLocation,
      this.themes});

  @override
  _LessonCardState createState() => _LessonCardState();
}

class _LessonCardState extends State<LessonCardWidget> {
  double _heightLine;
  double _heightFirst;
  double _heightSecond;
  Color _lineColor;
  Color _secondColor;


  @override
  void initState() {
    _heightLine = widget.isDual == true ? 200 : 100;
    _heightFirst = 100;
    _heightSecond = widget.isDual == true ? 100 : 0;
    _lineColor =
        widget.isNow == true ? Constants.blueAccent : Constants.greyAccent;
    _secondColor = widget.themes ? Constants.greyAccent : Constants.graytime;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
          cardTime(widget.timeStart, widget.timeEnd),
          SizedBox(width: 24),
          lineLesson(_heightLine, _lineColor),
          SizedBox(width: 16),
          Container(
              child: Column(children: <Widget>[
            cardLessen(
                _heightFirst,
                MediaQuery.of(context).size.width - 105,
                widget.firstLessonName,
                widget.firstLessonType,
                widget.firstLessonLocation,
                widget.firstLessonTeacher),
            cardLessen(
                _heightSecond,
                MediaQuery.of(context).size.width - 105,
                widget.secondLessonName,
                widget.secondLessonType,
                widget.secondLessonLocation,
                widget.secondLessonTeacher)
          ]))
        ]));
  }

  Widget cardLessen(double height, double width, String name, String type,
          String location, String teacher) =>
      Container(
          height: height,
          width: width,
          alignment: Alignment.centerLeft,
          child: Column(children: <Widget>[
            Container(
                alignment: Alignment.centerLeft,
                height: 100,
                child: ListView(
                    primary: false,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      Container(
                          child: Column(children: <Widget>[
                        Container(
                            alignment: Alignment.centerLeft,
                            child: Text(name,
                                style: TextStyle(
                                    fontWeight: FontWeight.w700, fontSize: 16),
                                maxLines: 2,
                                textAlign: TextAlign.left)),
                        SizedBox(height: 8),
                        Container(
                            alignment: Alignment.centerLeft,
                            child: Text(type,
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: _secondColor),
                                maxLines: 1,
                                textAlign: TextAlign.left)),
                        SizedBox(height: 4),
                        Row(children: <Widget>[
                          SizedBox(width: 3),
                          ClipRRect(
                              borderRadius: BorderRadius.circular(3),
                              child: Row(children: <Widget>[
                                Container(
                                    width: 19,
                                    height: 19,
                                    color: _lineColor,
                                    alignment: Alignment.center,
                                    child: Icon(Icons.location_on, size: 14, color: Constants.lightPrimary,)),
                                SizedBox(width: 4),
                                Container(
                                    color: _lineColor,
                                    height: 19,
                                    width: 74,
                                    alignment: Alignment.center,
                                    child: Text(location,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 11,
                                            color: Colors.white),
                                        maxLines: 1,
                                        textAlign: TextAlign.center))
                              ])),
                          SizedBox(width: 25),
                          Container(
                              alignment: Alignment.centerLeft,
                              constraints: BoxConstraints(maxWidth: 130),
                              child: Text(teacher,
                                  style: TextStyle(
//                                      fontWeight: FontWeight.normal,
                                      color: _secondColor,
                                      fontSize: 12),
                                  maxLines: 1,

                                  textAlign: TextAlign.left))
                        ])
                      ]))
                    ]))
          ]));

  Widget cardTime(String start, String end) => Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 12),
      child: Column(children: <Widget>[
        Container(
            alignment: Alignment.topLeft,
            child: Text(start,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    ),
                maxLines: 1,
                textAlign: TextAlign.left)),
        SizedBox(height: 40),
        Container(
            alignment: Alignment.centerLeft,
            child: Text(end,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: _secondColor),
                maxLines: 1,
                textAlign: TextAlign.left))
      ]));

  Widget lineLesson(height, Color c) => Container(
      height: height,
      margin: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      decoration: new BoxDecoration(
          border: Border(left: BorderSide(width: 3.0, color: c))));
}
