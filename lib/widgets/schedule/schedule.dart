import 'dart:developer';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

import 'package:inrtu_app/util/misc.dart';
import 'package:inrtu_app/util/storage.dart';
import 'package:inrtu_app/model/schedule_model.dart';
import 'package:inrtu_app/components/message_box.dart';
import 'package:inrtu_app/widgets/schedule/day_card.dart';

class ScheduleWidget extends StatefulWidget {
  final SharedPreferences sharedStorage;


  ScheduleWidget({Key key, @required this.sharedStorage}) : super(key: key);

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<ScheduleWidget>
    with SingleTickerProviderStateMixin {
  String maintitle = "";
  bool _weekIsOdd = true;
  bool _hasConnect = true;
  String _group;
  Future<GroupSchedule> _schedule;
  bool themedark;

  @override
  void initState() {
    try {
      _schedule =
          parseSchedule(widget.sharedStorage.getString('schedule') ?? "");
      themedark = widget.sharedStorage.getBool('dark') ?? true;
      setState(() {});
    } catch (FormatException) {
      throw Error();
    }

    checkConnect().then((value) {
      _hasConnect = value;
      setState(() {});
    });

    _group = widget.sharedStorage.getString('group') ?? "";
    getScheduleUpdate(_group).then((schedule) async {
      if (schedule.isEmpty) {
        if (widget.sharedStorage.getBool('needUpdate') ?? false) {
          final localSchedule = await getScheduleFromLocal(_group);
          widget.sharedStorage.setString('schedule', localSchedule);
        }
      } else {
        widget.sharedStorage.setString('schedule', schedule);
        FileStorage.writeLocalFile(_group, schedule);
      }

      try {
        _schedule =
            parseSchedule(widget.sharedStorage.getString('schedule') ?? "");
        setState(() {});
      } catch (FormatException) {
        throw Error();
      }
    }).catchError((_) {
      // Ошибка с получением с сервера(плохой json)
      log("server error(bad json)");
    });

    _weekIsOdd = getWeekOdd();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _group == "" ? maintitle = "Расписание" : maintitle = _group;

    return Scaffold(
      appBar: AppBar(
        title: new Text(maintitle),
        centerTitle: true,),
      body: ListView(
        children: <Widget>[
          Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                Container(
                    child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        child: RaisedButton(
                            child: new Text('Четная'),
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(10.0),
                                    bottomLeft: const Radius.circular(10.0))),
                            color: _weekIsOdd ? Colors.blue : Colors.white30,
                            onPressed: () =>
                                setState(() => _weekIsOdd = false)))),
                ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width / 2,
                    child: RaisedButton(
                        child: new Text('Нечетная'),
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.only(
                                topRight: const Radius.circular(10.0),
                                bottomRight: const Radius.circular(10.0))),
                        color: _weekIsOdd ? Colors.white30 : Colors.blue,
                        onPressed: () => setState(() => _weekIsOdd = true)))
              ])),
          FutureBuilder<GroupSchedule>(
            future: _schedule,
            builder: (context, snapshot) {
              if (_group.isEmpty) {
                return new MessageBoxWidget(
                    message: 'Перейдите в настройки и выберите группу');
              }
              if (snapshot.hasData) {
                return new Padding(
                    padding: EdgeInsets.all(0),
                    child: ListView.builder(
                        primary: false,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: snapshot.data.schedule == null ? 0 : snapshot.data.schedule.days.length,
                        itemBuilder: (BuildContext context, int index) {
                          if (snapshot.data.schedule.days[index].isEven == _weekIsOdd) {
                            return Padding(
                                padding: const EdgeInsets.only(bottom: 15.0),
                                child: DayCardWidget(snapshot: snapshot, index: index, themes : themedark));
                          }
                          return new SizedBox(height: 0);
                        }));
              }
              if (!_hasConnect) {
                return new MessageBoxWidget(
                    message: 'Отсутствует подключение к Интернету');
              }

              return new  SpinKitDoubleBounce(
                color: Colors.blueAccent,
                size: 50.0,

              );
            },
          ),
        ],
      ),
    );
  }

  bool getWeekOdd() {
    final date = new DateTime.now();
    final startOfYear = new DateTime(date.year, 1, 1, 0, 0);
    final daysInFirstWeek = 8 - startOfYear.weekday;
    var weeks =
        ((date.difference(startOfYear).inDays - daysInFirstWeek) / 7).ceil();
    if (daysInFirstWeek > 3) {
      weeks += 1;
    }
    weeks = weeks + 1;
    return weeks.isOdd;
  }
}
