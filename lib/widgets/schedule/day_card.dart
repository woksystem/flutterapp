import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:inrtu_app/model/schedule_model.dart';
import 'package:inrtu_app/util/constants.dart';

import 'package:inrtu_app/components/curve_painter.dart';
import 'lesson_card.dart';

class DayCardWidget extends StatefulWidget {
  final AsyncSnapshot<GroupSchedule> snapshot;
  final int index;
  final bool themes;

  DayCardWidget({this.snapshot, this.index, this.themes});

  @override
  _DayCardState createState() => _DayCardState();
}

class _DayCardState extends State<DayCardWidget> {


  @override
  Widget build(BuildContext context) {
    var assetsImage = new AssetImage('assets/teacup.png'); //<- Creates an object that fetches an image.
    var image = new Image(image: assetsImage, fit: BoxFit.cover,); //<- Creates a widget that displays an image.

    return new FutureBuilder(builder:
        (BuildContext context, AsyncSnapshot<GroupSchedule> feedState) {
      if (widget.snapshot.data == null) {
        return new Center(child: new CircularProgressIndicator());
      }
      return new Card(
          child: Column(children: <Widget>[
        Container(
            padding: const EdgeInsets.only(left: 12.0, top: 12.0, bottom: 14.0, right: 12.0),
            child: Row(children: <Widget>[
              SizedBox(width: 5),
              Container(width: 9, child: CustomPaint(painter: DrawCircle())),
              SizedBox(width: 5),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      "${widget.snapshot.data.schedule.days[widget.index].name}",
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 16,
                          color: widget.themes ? Constants.greyAccent : Constants.graytime),
                      maxLines: 1,
                      textAlign: TextAlign.left))
            ])),

            Container(
              margin: widget.snapshot.data.schedule.days[widget.index].lessons.length == 0 ? const EdgeInsets.all(15.0) : const EdgeInsets.all(0.0) ,
              height: widget.snapshot.data.schedule.days[widget.index].lessons.length == 0 ? 110 : 0,
                child: image,
            ),

            Container(
              margin: widget.snapshot.data.schedule.days[widget.index].lessons.length == 0 ? const EdgeInsets.all(15.0) : const EdgeInsets.all(0.0) ,
              height: widget.snapshot.data.schedule.days[widget.index].lessons.length == 0 ? 20 : 0,
              child:  Text(
                  "Занятий нет, отдыхай)",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 16,
                      color: widget.themes ? Constants.greyAccent : Constants.graytime),
                  maxLines: 1,
                  textAlign: TextAlign.left)
            ),


            Container(
            child: ListView.builder(
                primary: false,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.snapshot.data.schedule.days[widget.index].lessons == null ? 0 : widget.snapshot.data.schedule.days[widget.index].lessons.length,
                itemBuilder: (BuildContext context, int indexLow) {

                  return Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Column(children: <Widget>[

                        LessonCardWidget(
                            timeStart: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].timeStart,
                            timeEnd: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].timeEnd,
                            firstLessonName: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].firstLessonName,
                            secondLessonName: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].secondLessonName,
                            firstLessonType: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].firstLessonType,
                            secondLessonType: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].secondLessonType,
                            firstLessonLocation: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].firstLessonLocation,
                            secondLessonLocation: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].secondLessonLocation,
                            firstLessonTeacher: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].firstLessonTeacher,
                            secondLessonTeacher: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].secondLessonTeacher,
                            isDual: widget.snapshot.data.schedule.days[widget.index].lessons[indexLow].isDual,
                            isNow: true,
                            themes: widget.themes,
                        ),
                        SizedBox(height: 15),
                        Container(
                            width: widget.snapshot.data.schedule.days[widget.index].lessons.length - 1 == indexLow ? 0 : MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(border: Border(bottom: BorderSide(width: 1, color: Colors.blueGrey))))
                      ]));
                }))
      ]));
    });
  }
}
