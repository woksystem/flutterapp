import 'package:flutter/material.dart';
import 'package:inrtu_app/widgets/eventsNews/categories.dart';

class AboutWidget extends StatefulWidget {
  final Map newsItem;

  const AboutWidget({Key key, this.newsItem}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<AboutWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
            actions: <Widget>[]),
        body: ListView(children: <Widget>[
//          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(3.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Приложение разработано в рамках Гранта Ученого Совета ИРНИТУ",
              style: TextStyle(
//                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
//              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.all(3.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Контакты для связи с разработчиками:",
              style: TextStyle(
//                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
//              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          Container(

            alignment: Alignment.centerLeft,
            child: Text(
              "jumpflayer@gmail.com",
              style: TextStyle(
//                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
//              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
        ]));
  }
}
