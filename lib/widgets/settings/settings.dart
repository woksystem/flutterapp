import 'dart:developer';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:inrtu_app/util/custom_theme.dart';
import 'package:inrtu_app/widgets/settings/about.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:inrtu_app/util/constants.dart';
import 'package:inrtu_app/model/groups_model.dart';

class SettingsWidget extends StatefulWidget {
  final SharedPreferences sharedStorage;

  SettingsWidget({Key key, @required this.sharedStorage}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsWidget> {
  bool isSwitched;
  String _group = "";
  GlobalKey<AutoCompleteTextFieldState<String>> _key = new GlobalKey();
  List<String> _suggestions = [];
  SimpleAutoCompleteTextField _textField;

  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  void initState() {
    isSwitched = widget.sharedStorage.getBool('dark') ?? true;
    final groups = widget.sharedStorage.getString('groups') ?? "";
    _group = widget.sharedStorage.getString('group') ?? "";
    try {
      log("parse: groups");
      _suggestions = parseGroups(groups).groups;
    } catch (Exception) {
      log("ex: ");
    }

    _textField = SimpleAutoCompleteTextField(
        key: _key,
        controller: TextEditingController(text: _group),
        decoration: new InputDecoration(hintText: "Группа:"),
        suggestions: _suggestions,
//        focusNode: unfocus(),
        clearOnSubmit: false,
        textSubmitted: (text) => setState(() {
              if (text.isNotEmpty) {
                _group = text;
              }
            }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar: AppBar(
        title: new Text("Настройки"),
        centerTitle: true),

      body:  Column(children: [
        ListTile(
          title: Text("Учебная группа:"),
//          subtitle: Text("Subtitle goes here"),
          onTap: (){},
        ),
        new ListTile(
            title: _textField,
            trailing: new IconButton(
                icon: new Icon(Icons.add),
                onPressed: () {
                  if (!_suggestions.contains(_group)) {
                    _textField.updateDecoration(
                        decoration:
                        new InputDecoration(errorText: "Группа не найдена"));
                    return;
                  }
                  widget.sharedStorage.setString('group', _group);
                  widget.sharedStorage.setBool('needUpdate', true);

                  _textField.updateDecoration(
                      decoration:
                      new InputDecoration(helperText: "Группа добавлена"));
                })),
        SizedBox(height: 15,),
        SwitchListTile(
          value: isSwitched,
          title: Text("Тёмная тема"),
          onChanged: (value) => setState(() {
            if(isSwitched){
              widget.sharedStorage.setBool('dark', value);
            }
            else{
              widget.sharedStorage.setBool('dark', value);
            }
            isSwitched = value;
            _changeTheme(context, value ? MyThemeKeys.DARK : MyThemeKeys.LIGHT );

          },
          ),
          activeColor: Colors.blueAccent,
        ),
        SizedBox(height: 15),
        ListTile(
          title: Text("О приложении"),
//          subtitle: Text("Subtitle goes here"),
          onTap: (){
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) {
                  return AboutWidget();
                },
              ),
            );
          },
        ),

          ]
       ),

      );



  }
}
