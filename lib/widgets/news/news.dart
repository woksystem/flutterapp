import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inrtu_app/model/news_model.dart';
import 'package:inrtu_app/widgets/eventsNews/categories.dart';
import 'package:inrtu_app/widgets/news/news_details.dart';
import 'package:inrtu_app/components/message_box.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NewsWidget extends StatefulWidget {
  List<String> reportList;

  NewsWidget({this.reportList});
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<NewsWidget> {

  Future<List<dynamic>> _news;
  bool _hasConnect = false;

  @override
  void initState() {
    InternetAddress.lookup('google.com').then((value) {
      try {
          if (value.isNotEmpty && value[0].rawAddress.isNotEmpty) {
            _hasConnect = true;
            _news = getNewsFromServer();
            setState(() {});
          }
      } catch (Exception) {
        log("can nit load news");
      }
    }).catchError((error) {
      _hasConnect = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
//      SizedBox(height: 40),
          FutureBuilder<List<dynamic>>(
              future: _news,
              builder: (context, snapshot) {
                if (!_hasConnect) {
                  return new MessageBoxWidget(
                      message: 'Отсутствует подключение к Интернету');
                }
                if (snapshot.hasData) {
                  return new Container(
//                          padding: EdgeInsets.only( left: 10),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      primary: false,
                      itemCount: snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {

                          Map newsItem = snapshot.data[index];
                          bool flag = false;
                          widget.reportList.length == 0 ? flag = true : log("not null");
                          newsItem["categories"].forEach((item) => widget.reportList.contains(item) ? flag = true : log("no cat"));
                          if(flag) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Container(
//                              color: Colors.black12,
                              child: InkWell(
                                child: Container(
                                  height: 377,
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  child: Column(
                                    children: <Widget>[
                                      ClipRRect(
//                                            borderRadius: BorderRadius.circular(5),
                                        child: Image.network(
                                          "${newsItem["header_image"]}",
                                          height: 170,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      SizedBox(height: 7),
                                      Container(
                                        height: 25,
                                        child: CategoriesWidget(
                                            snapshot: newsItem["categories"]),

                                      ),
//                                          SizedBox(height: 7),
                                      Container(
                                        padding: EdgeInsets.all(3.0),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "${newsItem["title"]}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          maxLines: 2,
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
//                                          SizedBox(height: 3),
                                      Container(
                                        padding: EdgeInsets.all(3.0),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "${newsItem["content"]}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: Colors.blueGrey[300],
                                          ),
                                          maxLines: 4,
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
//                                          SizedBox(height: 10),
                                      Container(
                                        padding: EdgeInsets.all(3.0),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "${newsItem["date"]}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10,
                                          ),
                                          maxLines: 1,
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) {
                                        return NewsDetailsWidget(
                                            newsItem: newsItem);
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                          );
                        };
                          return  SizedBox(height: 0);
                      },
                    ),
                  );
                }
                if (snapshot.hasError) {
                  return new Text("${snapshot.error}");
                }
                return new SpinKitDoubleBounce(
                  color: Colors.blueAccent,
                  size: 50.0,
                );
              }),
    ]));
  }
}
