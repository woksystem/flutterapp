import 'package:flutter/material.dart';
import 'package:inrtu_app/widgets/eventsNews/categories.dart';

class NewsDetailsWidget extends StatefulWidget {
  final Map newsItem;

  const NewsDetailsWidget({Key key, this.newsItem}) : super(key: key);

  @override
  _NewsDetailsState createState() => _NewsDetailsState();
}

class _NewsDetailsState extends State<NewsDetailsWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
            actions: <Widget>[]),
        body: ListView(children: <Widget>[
//          SizedBox(height: 10),
          Container(
            height: 250,
            width: MediaQuery.of(context).size.width,

            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: NetworkImage("${widget.newsItem["header_image"]}"),
              ),
            ),
          ),

          SizedBox(height: 10),
          ListView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width - 40,
                          alignment: Alignment.centerLeft,
                          child: Text("${widget.newsItem["title"]}",
                              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
                              textAlign: TextAlign.left)),
                    ]),
                SizedBox(height: 20),
                Container(
                  height: 25,
                  child: CategoriesWidget(snapshot: widget.newsItem["categories"]),

                ),
                SizedBox(height: 10),
                Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.centerLeft,
                    child: Text("${widget.newsItem["content"]}",
                        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
                        textAlign: TextAlign.left)),
                SizedBox(height: 10)
              ])
        ]));
  }
}
