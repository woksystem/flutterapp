import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inrtu_app/components/icon_badge.dart';
import 'package:inrtu_app/widgets/events/events.dart';
import 'package:inrtu_app/widgets/eventsNews/multi_select.dart';
import 'package:inrtu_app/widgets/news/news.dart';

class NewsEventWidget extends StatefulWidget {
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<NewsEventWidget> {
  bool _hasConnect = false;

  bool isLoading = false;
  List<String> reportList = [
    "Воспитание, культура, спорт, здоровье",
    "Международная деятельность",
    "Инновации",
    "Наука",
    "Об университете",
    "Абитуриентам",
    "Общественная жизнь",
    "Образование",
    "Технопарк",
    "Расписание",
    "Пресс служба",
    "Студентам",
    "Культура",
    "Сотрудникам и преподавателям",
    "Выпускникам и работодателям"
  ];

  List<String> selectedReportList = List();

  @override
  void initState() {
    InternetAddress.lookup('google.com').then((value) {
      try {
          if (value.isNotEmpty && value[0].rawAddress.isNotEmpty) {
            _hasConnect = true;

            setState(() {});
          }
      } catch (Exception) {
        log("can nit load news");
      }
    }).catchError((error) {
      _hasConnect = false;
    });
    super.initState();
    //  invoking api calls when the app started to build ****

  }

  _showReportDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //Here we will build the content of the dialog
          return AlertDialog(
            title: Text("Выберите категории"),
            content: MultiSelectChip(
              reportList,

              onSelectionChanged: (selectedList) {

                setState(() {
                  selectedReportList = selectedList;
                });
              },
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Выбрать"),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }



  @override
  Widget build(BuildContext context) {
    //  Default Tab controller
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(

        appBar: AppBar(
          title: Text("Новости и события "),
          actions: <Widget>[
          IconButton(
              icon: IconBadgeWidget(icon: Icons.sort),
              onPressed: () => _showReportDialog(),
          ),
          ],
          centerTitle: true,
          elevation: 0,

          //  Tabbar in appbar
          bottom: TabBar(
//            isScrollable: true,
            indicatorColor: Colors.blueAccent,

//            indicatorPadding: const EdgeInsets.all(2.0),
            //  The size of the indicator is same as the size of the text
            // indicatorSize: TabBarIndicatorSize.label,
//            indicatorWeight: 5.0,

            tabs: <Widget>[

              Tab(
                child: Container(
                  child: Text("Новости"),
                ),
              ),
              Tab(
                child: Container(
                  child: Text("События"),
                ),
              ),
            ],
          ),
        ),

        body: Stack(
          children: <Widget>[
            //  Building TabBarView for showing the news in the body
            TabBarView(
              children: <Widget>[
                //  RefreshIndicator is used to implement the functionality of Swipe down to refresh it requires a GlobalKey and onRefresh callback
                Container(
                  child: NewsWidget(reportList: selectedReportList),
//                    child: Text(selectedReportList.join(" , ")),
                ),
                Container(
                  child: EventWidget(),
                ),

              ],
            ),
          ],
        ),
      ),
    );
  }

}
