import 'package:flutter/material.dart';


class CategoriesWidget extends StatefulWidget {
  final List<dynamic> snapshot;


  CategoriesWidget({this.snapshot});

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<CategoriesWidget> {
  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(

        builder: (context, snapshot) {

      return
            Container(
                child: ListView.builder(
//                    primary: false,
//                    physics: NeverScrollableScrollPhysics(),
//                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.snapshot == null ? 0 : widget.snapshot.length,
                    itemBuilder: (BuildContext context, int indexLow) {
                      return Row(children: <Widget>[
                            SizedBox(width: 5),
                            Container(
                              padding: EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 2,

                                ),
                                borderRadius: new BorderRadius.circular(5.0)
                              ),

                              child: Text(
                                widget.snapshot[indexLow],
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Colors.blue,
                                ),

                                maxLines: 1,
//                                textAlign: TextAlign.justify,
                              ),
                            ),

                            ]);
                    }));

    });
  }
}
