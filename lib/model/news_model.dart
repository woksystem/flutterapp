import 'dart:io';

import 'dart:convert' show json;
import 'package:inrtu_app/util/http_client.dart';
import 'package:inrtu_app/util/misc.dart';

Future<List<dynamic>> parseNews(String data) async {
  final Map<String, dynamic> newsJson = json.decode(data);
  for (var item in newsJson['news']) {
//    String stringCategories = "";
//    for (var category in item['categories']) {
//      stringCategories += category + " ";
//    }
//    item['categories'] = stringCategories;

    String stringContent = "";
    for (var line in item['content']) {
      stringContent += line +"\n"+"\n";
    }
    item['content'] = stringContent;
  }
  return newsJson['news'];
}

Future<List<dynamic>> getNewsFromServer() async {
  String news;
  for (var i = 0; i < 5; ++i) {
    await HttpRequest.getNews().then((response) {
      if (response != null ||
          response.code == 200 ||
          !isEmptyJson(response.body)) {
        news = response.body;
      }
    }).catchError((_) {});

    if (news != null && news.isNotEmpty) {
      return await parseNews(news);
    }
    sleep(Duration(seconds: 1));
  }

  throw new Exception();
}
