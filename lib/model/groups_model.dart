import 'dart:convert' show json;
import 'package:inrtu_app/util/misc.dart' show isEmptyJson;
import 'package:inrtu_app/util/http_client.dart';
import 'dart:io' show sleep;

class Groups {
  final String hash;
  final List<String> groups;

  Groups({this.hash, this.groups});

  Groups.fromJson(Map<String, dynamic> data)
      : hash = data['hash'],
        groups = new List<String>.from(data['groups']);
}

Groups parseGroups(String data) {
  return new Groups.fromJson(json.decode(data));
}

Future<String> getGroupsUpdate(String hash) async {
  try {
    final groups = await _getGroupsFromServer(hash);

    return groups.isEmpty ? "" : groups;
  } catch (Exception) {
    return "";
  }
}

Future<String> _getGroupsFromServer(String hash) async {
  String groups;
  for (var i = 0; i < 5; ++i) {
    await HttpRequest.getGroups(hash).then((response) {
      if (response != null ||
          response.code == 200 ||
          !isEmptyJson(response.body)) {
        groups = response.body;
      }
    }).catchError((_) {});

    if (groups != null && groups.isNotEmpty) {
      return groups;
    }
    sleep(Duration(seconds: 1));
  }

  throw new Exception();
}
