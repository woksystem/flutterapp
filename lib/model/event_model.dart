import 'dart:io';

import 'dart:convert' show json;
import 'package:inrtu_app/util/http_client.dart';
import 'package:inrtu_app/util/misc.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'package:intl/intl.dart';



Future<List<dynamic>> parseEvent(String data) async {
  final String defaultimg = "https://postupi.online/images/images1366/34/765.jpg";
  initializeDateFormatting("ru", null);
  final Map<String, dynamic> eventJson = json.decode(data);
  for (var item in eventJson['values']) {

    String stringImg = "";
    stringImg = item['poster_image']== null ? defaultimg  : item['poster_image']['default_url'] ;


    item['poster_image'] = stringImg;

    String endTime = "";
    endTime = item["ends_at"]==null ? (DateTime.parse(item['starts_at']).add(Duration(hours: 2))).toString() : item["ends_at"];

    List<String> cats = [];

    for (var category in item['categories']) {
      cats.add(category['name']);
    }
    item['categories'] = cats;
    var time = DateTime.parse(item['starts_at']);
    var newtime = new DateTime(time.year, time.month, time.day, time.hour + 3, time.minute);

    item['ends_at']= endTime;
    item['starts_at_new']= new DateFormat.yMMMd('ru').add_jm().format(newtime);

  }
  return eventJson['values'];
}

Future<List<dynamic>> getEventFromServer() async {
  String event;
  for (var i = 0; i < 5; ++i) {
    await HttpRequest.getEvents().then((response) {
      if (response != null ||
          response.code == 200 ||
          !isEmptyJson(response.body)) {
        event = response.body;
      }
    }).catchError((_) {});

    if (event != null && event.isNotEmpty) {
      return await parseEvent(event);
    }
    sleep(Duration(seconds: 1));
  }

  throw new Exception();
}
