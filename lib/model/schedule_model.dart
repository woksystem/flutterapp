import 'dart:convert' show json;
import 'dart:async' show Future;
import 'dart:developer';
import 'dart:io' show sleep;
import 'package:inrtu_app/util/storage.dart';
import 'package:inrtu_app/util/http_client.dart';
import 'package:inrtu_app/util/misc.dart' show isEmptyJson;
import 'package:inrtu_app/util/misc.dart' show readHash;

class GroupSchedule {
  final String hash;
  final Schedule schedule;

  GroupSchedule({this.hash, this.schedule});

  GroupSchedule.fromJson(Map<String, dynamic> data)
      : hash = data['hash'],
        schedule = Schedule.fromJson(data['schedule']);
}

class Schedule {
  final String period;
  final List<Day> days;

  Schedule({this.period, this.days});

  Schedule.fromJson(Map<String, dynamic> data)
      : period = data['period'],
        days = (data['days'] as List).map((day) => Day.fromJson(day)).toList();
}

class Day {
  final String name;
  final bool isEven;
  final List<Lesson> lessons;

  Day({this.name, this.isEven, this.lessons});

  Day.fromJson(Map<String, dynamic> parsedJson)
      : name = parsedJson['name'],
        isEven = parsedJson['isEven'],
        lessons = (parsedJson['lessons'] as List)
            .map((lesson) => Lesson.fromJson(lesson))
            .toList();
}

class Lesson {
  final bool isDual;
  final String firstLessonName;
  final String secondLessonName;
  final String timeStart;
  final String timeEnd;
  final String firstLessonType;
  final String secondLessonType;
  final String firstLessonTeacher;
  final String secondLessonTeacher;
  final String firstLessonLocation;
  final String secondLessonLocation;

  Lesson(
      {this.isDual,
      this.firstLessonName,
      this.secondLessonName,
      this.timeStart,
      this.timeEnd,
      this.firstLessonType,
      this.secondLessonType,
      this.firstLessonTeacher,
      this.secondLessonTeacher,
      this.firstLessonLocation,
      this.secondLessonLocation});

  Lesson.fromJson(Map<String, dynamic> parsedJson)
      : isDual = parsedJson['isDual'],
        firstLessonName = parsedJson['f_name'] == ""
            ? parsedJson['s_name']
            : parsedJson['f_name'],
        secondLessonName = parsedJson['s_name'],
        timeStart = parsedJson['t_start'],
        timeEnd = parsedJson['t_end'],
        firstLessonType = parsedJson['f_type'] == ""
            ? parsedJson['s_type']
            : parsedJson['f_type'],
        secondLessonType =
            parsedJson['s_type'] == "" ? "" : parsedJson['s_type'],
        firstLessonTeacher = parsedJson['f_teacher'] == ""
            ? parsedJson['s_teacher']
            : parsedJson['f_teacher'],
        secondLessonTeacher = parsedJson['s_teacher'],
        firstLessonLocation = parsedJson['f_location'] == ""
            ? parsedJson['s_location']
            : parsedJson['f_location'],
        secondLessonLocation = parsedJson['s_location'];
}

Future<GroupSchedule> parseSchedule(String data) async {
  return new GroupSchedule.fromJson(json.decode(data));
}

/// Если обновлений нет, возвращает пустую строку
Future<String> getScheduleUpdate(String group) async {
  final hash = await readHash(group);
  final schedule = await _getScheduleFromServer(group, hash);
  log("hash " + hash);
  return schedule.isEmpty ? "" : schedule;
}

Future<String> getScheduleFromLocal(String group) async {
  String schedule;
  await FileStorage.readLocalFile(group).then((text) async {
    schedule = text;
  }).catchError((_) {});
  return schedule;
}

Future<String> _getScheduleFromServer(String group, String hash) async {
  String schedule;
  for (var i = 0; i < 5; ++i) {
    await HttpRequest.getSchedule(hash, group).then((response) {
      if (response != null &&
          response.code == 200 &&
          !isEmptyJson(response.body)) {
        schedule = response.body;
      }
    }).catchError((_) {});

    if (schedule != null && schedule.isNotEmpty) {
      return schedule;
    }
    sleep(Duration(milliseconds: 1));
  }

  return "";
}
