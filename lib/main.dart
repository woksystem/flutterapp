 import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inrtu_app/util/custom_theme.dart';
import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

import 'package:inrtu_app/widgets/main_screen.dart';
import 'package:inrtu_app/util/constants.dart';
import 'package:inrtu_app/model/groups_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedStorage = await SharedPreferences.getInstance();
  sharedStorage.setBool('needUpdate', false);
  bool _isDark = sharedStorage.getBool('dark') ?? true;

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(
        CustomTheme(
            initialThemeKey: _isDark ? MyThemeKeys.DARK : MyThemeKeys.LIGHT,
            child: MyApp(sharedStorage: sharedStorage)
        ),
    );
  });
}

class MyApp extends StatefulWidget {
  final SharedPreferences sharedStorage;

  MyApp({Key key, @required this.sharedStorage}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool _isDark;
  @override
  void initState() {
    super.initState();
//    _isDark = widget.sharedStorage.getBool('dark') ?? false;
    _isDark = true;

    final groupsHash = widget.sharedStorage.getString('groupsHash') ?? "";
    getGroupsUpdate(groupsHash).then((groups) {
      if (groups.isEmpty) {
        return;
      }

      try {
        var newGroups = parseGroups(groups);
        widget.sharedStorage.setString('groups', groups);
        widget.sharedStorage.setString('groupsHash', newGroups.hash);

      } catch (Exception) {
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: Constants.appName,
        theme: CustomTheme.of(context),
        home: MainScreen(sharedStorage: widget.sharedStorage));
  }
}
