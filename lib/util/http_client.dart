import 'dart:io';

import 'package:http/http.dart' as http;

class HttpResponse {
  final String body;
  final int code;

  HttpResponse(this.body, this.code);
}

class HttpRequest {
  static final String _serverUrl = "http://178.62.241.81:8080";
  static final String _groupsUrl = "$_serverUrl/groups";
  static final String _scheduleUrl = "$_serverUrl/schedule";
  static final String _newsUrl = "$_serverUrl/news";
  static final String _eventUrl = "https://api.timepad.ru/v1/events?organization_ids=112730&fields=description_short, description_html, poster_image, ends_at&limit=50&sort=starts_at";


  static Future<HttpResponse> _httpGet(String url) async {
    try {
      final response = await http.get(url);
      return new HttpResponse(response.body, response.statusCode);
    } catch (e) {
      return null;
    }
  }
  static Future<HttpResponse> _httpGetTimePad(String url) async {
    try {
      final response = await http.get(url, headers: {HttpHeaders.authorizationHeader: "Bearer ab698bfaa037feced22cb7d6370af63a4f524e2f"},);
      return new HttpResponse(response.body, response.statusCode);
    } catch (e) {
      return null;
    }
  }

  static Future<HttpResponse> getGroups(String hash) async {
    return await _httpGet("$_groupsUrl?hash=$hash");
  }

  static Future<HttpResponse> getSchedule(String hash, String group) async {
    return await _httpGet("$_scheduleUrl?hash=$hash&group=$group");
  }

  static Future<HttpResponse> getNews() async {
    return await _httpGet("$_newsUrl");
  }

  static Future<HttpResponse> getEvents() async {
    return await _httpGetTimePad("$_eventUrl");
  }
}
