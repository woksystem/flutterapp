import 'package:path_provider/path_provider.dart';
import 'dart:io';

class FileStorage {
  static Future<String> get _localPath async {
    return (await getApplicationDocumentsDirectory()).path;
  }

  static Future<File> _getLocalFile(String filename) async {
    final path = await _localPath;
    return File('$path/$filename.json');
  }

  static Future<File> _createLocalFile(String filename) async {
    final path = await _localPath;
    return File('$path/$filename.json').create();
  }

  static Future<String> readLocalFile(String filename) async {
    try {
      final file = await _getLocalFile(filename);
      return await file.readAsString();
    } catch (e) {
      _createLocalFile(filename);
      return "";
    }
  }

  static Future<File> writeLocalFile(String filename, String data) async {
    final file = await _getLocalFile(filename);
    return file.writeAsString('$data');
  }
}
