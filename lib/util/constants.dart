import 'package:flutter/material.dart';

enum MyThemeKeys {LIGHT, DARK}

class Constants {
  static String appName = "ИрНИТУ";

  //Colors for theme
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Color(0xFF0D0D0D);
  static Color lightAccent = Colors.blueGrey[900];
  static Color darkAccent = Colors.white;
  static Color lightBG = Color(0xfffcfcff);
  static Color darkBG = Colors.black;
  static Color badgeColor = Colors.red;
  static Color blueAccent = Colors.blue;
  static Color greyAccent = Colors.white70;
  static Color lightCard = Colors.white;
  static Color darkCard = Color(0xFF202020);
  static Color graytime = Colors.blueGrey[800];

  static final ThemeData lightTheme = ThemeData(
      backgroundColor: lightBG,
      primaryColor: lightPrimary,
      accentColor: lightAccent,
      cursorColor: lightAccent,
      cardColor: lightCard,
      scaffoldBackgroundColor: lightBG,
      appBarTheme: AppBarTheme(
          elevation: 0,
          textTheme: TextTheme(
              title: TextStyle(
                  color: darkBG,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w800))));

  static final ThemeData darkTheme = ThemeData(
      brightness: Brightness.dark,
      backgroundColor: darkBG,
      primaryColor: darkPrimary,
      accentColor: darkAccent,
      cardColor: darkCard,
      scaffoldBackgroundColor: darkBG,
      cursorColor: darkAccent,
      appBarTheme: AppBarTheme(
          elevation: 0,
          textTheme: TextTheme(
              title: TextStyle(
                  color: lightBG,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w800))));

  static ThemeData getThemeFromKey(MyThemeKeys themeKey) {
    switch (themeKey) {
      case MyThemeKeys.LIGHT:
        return lightTheme;
      case MyThemeKeys.DARK:
        return darkTheme;
      default:
        return darkTheme;
    }
  }
}
