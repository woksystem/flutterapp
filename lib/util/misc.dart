import 'dart:io';
import 'dart:convert' show json;
import 'dart:async';

import 'package:inrtu_app/util/storage.dart';

bool isEmptyJson(String val) {
  return json.decode(val).toString() == "{}";
}

/// Can return <null>
String parseHash(String data) {
  Map<String, dynamic> jsonResponse = json.decode(data);
  return jsonResponse.containsKey('hash') ? jsonResponse['hash'] : null;
}

Future<String> readHash(String file) async {
  final text = await FileStorage.readLocalFile(file);
  return text.isEmpty ? "" : parseHash(text);
}

Future<bool> checkConnect() async {
  bool hasConnect = false;
  try {
    final value = await InternetAddress.lookup('google.com');
    if (value.isNotEmpty && value[0].rawAddress.isNotEmpty) {
      hasConnect = true;
    }
  } on SocketException {}
  return hasConnect;
}
