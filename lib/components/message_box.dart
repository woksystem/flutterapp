import 'package:flutter/material.dart';

class MessageBoxWidget extends StatefulWidget {
  final String message;

  MessageBoxWidget({Key key, @required this.message}) : super(key: key);

  @override
  _MessageBoxState createState() => _MessageBoxState();
}

class _MessageBoxState extends State<MessageBoxWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            elevation: 10,
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              ListTile(
                  title: Text(widget.message,
                      style: TextStyle(color: Colors.white)))
            ])));
  }
}
